# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import datetime, time, pprint
from bs4 import BeautifulSoup


class EbayComCrawler():


	browser = None


	def __init__(self):
		try:
			PROXY = None

			chrome_options = Options()
			# chrome_options.add_argument("--headless")
			chrome_options.add_argument("--incognito")
			chrome_options.add_argument("--disable-gpu")
			chrome_options.add_argument("--disable-web-security")
			chrome_options.add_experimental_option("prefs", { "profile.managed_default_content_settings.images": 2 })

			if PROXY != None:
				chrome_options.add_argument("--proxy-server=%s" % PROXY)

			self.browser = webdriver.Chrome(executable_path=r"chromedriver.exe", chrome_options=chrome_options)
		except Exception as err:
			print("INIT-ERR: " + str(err))




	def _open(self, url):
		try:
			self.browser.get(url)
			self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
			self.browser.execute_script("$('#custLink').trigger('click');")
			time.sleep(2)
			self.browser.execute_script("$('#e1-13').trigger('click');")
			self.browser.execute_script("$('#e1-11').trigger('click');")
			self.browser.execute_script("$('[name=_fcippl]').trigger('click');")
			self.browser.execute_script("$('#e1-3').trigger('click');")
			time.sleep(3)
			self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

		except Exception as err:
			print("OPEN-ERR: " + str(err))




	def _download(self):
		try:
			return self.browser.page_source.encode("utf8","ignore")
		except Exception as err:
			print("DOWNLOAD-ERR: " + str(err))




	def _collect(self, html):
		try:
			soup = BeautifulSoup(html, "html.parser")

			raw = { "posts": [], "next": None }

			posts = soup.select("ul#ListViewInner > li")
			for post in posts:
				_post = {
					"title": None,
					"img": None,
					"url": None,
					"price": None,
					"date": None,
					"seller": None,
					"item": None,
				}


				try:
					_post["title"] = post.select("h3 > a")[0].getText().strip()
				except Exception as parseErr:
					print("title-ERR: " + str(parseErr))


				try:
					_post["img"] = post.select("img.img")[0].attrs["src"]
				except Exception as parseErr:
					print("img-ERR: " + str(parseErr))


				try:
					_post["url"] = post.select("h3 > a")[0].attrs["href"]
				except Exception as parseErr:
					print("url-ERR: " + str(parseErr))


				try:
					_post["price"] = post.select("li.lvprice.prc")[0].getText().strip()
				except Exception as parseErr:
					print("price-ERR: " + str(parseErr))


				try:
					_post["date"] = post.select("ul.lvdetails > li")[0].getText().strip()
				except Exception as parseErr:
					print("date-ERR: " + str(parseErr))


				try:
					_post["seller"] = post.select("ul.lvdetails > li")[1].getText().strip()
				except Exception as parseErr:
					print("seller-ERR: " + str(parseErr))


				try:
					_post["item"] = post.select("ul.lvdetails > li")[2].getText().strip()
				except Exception as parseErr:
					print("item-ERR: " + str(parseErr))


				raw["posts"].append(_post)

			try:
				pages = soup.select("a.pg")
				for i in range(0, len(pages)):
					if "curr" in pages[i].attrs["class"]:
						_next = i + 1
						if _next < len(pages):
							raw["next"] = pages[_next].attrs["href"]
							break
			except Exception as nextErr:
				print("NEXT-ERR: " + str(nextErr))

			return raw

		except Exception as err:
			print("COLLECT-ERR: " + str(err))




	def _redirect(self, url):
		self.browser.get(url)
		time.sleep(2)




	def _quit(self):
		self.browser.quit()




if __name__ == "__main__":

	browser = EbayComCrawler()
	browser._open("https://www.ebay.com/sch/m.html?_ssn=perfectmall2017&_sop=10")

	_today = datetime.datetime.now()
	new_posts = []

	while True:
		try:
			html = browser._download()
			posts = browser._collect(html)
			_hasOldPost = False

			for post in posts["posts"]:
				try:
					_date = post["date"].split(" ")[0] + " " + str(_today.year)
					_post = datetime.datetime.strptime(_date, '%b-%d %Y')

					range = (datetime.datetime.now() - _post)
					if range <= datetime.timedelta(days=7) and range >= datetime.timedelta(days=1):
						print(range)
						new_posts.append(post)
					else:
						_hasOldPost = True
						break
				except Exception as err1:
					print("ERR1: " + str(err1))

			if posts["next"] != None and _hasOldPost == False:
				browser._redirect(posts["next"])
			else:
				break
		except Exception as err:
			print("ERR: " + str(err))

	pprint.pprint(new_posts)
	# browser._quit()